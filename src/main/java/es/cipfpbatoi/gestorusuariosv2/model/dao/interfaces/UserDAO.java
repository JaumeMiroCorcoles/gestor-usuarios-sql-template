package es.cipfpbatoi.gestorusuariosv2.model.dao.interfaces;

import es.cipfpbatoi.gestorusuariosv2.model.entities.User;

import java.util.List;

public interface UserDAO {

    List<User> findAll();

    User findById(int id);

    User findByDNI(String dni);

    boolean save(User user);

    User findByEmail(String email);

    boolean remove(User user);

}
