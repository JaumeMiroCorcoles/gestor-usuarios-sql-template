package es.cipfpbatoi.gestorusuariosv2.model.dao;

import es.cipfpbatoi.gestorusuariosv2.model.dao.interfaces.UserDAO;
import es.cipfpbatoi.gestorusuariosv2.model.entities.User;
import es.cipfpbatoi.gestorusuariosv2.utils.database.MySQLConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;

@Service
public class SQLUserDAO implements UserDAO {

    private final MySQLConnection mySQLConnection;

    public SQLUserDAO(@Autowired MySQLConnection mySQLConnection) {
        this.mySQLConnection = mySQLConnection;
    }

    /**
     * Devuelve todos los usuarios almacenados en la Base de datos
     *
     * @return
     */
    @Override
    public List<User> findAll() {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo id coincide con @id
     *
     * @param id
     * @return
     */
    @Override
    public User findById(int id) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo dni coincide con @dni
     *
     * @param dni
     * @return User
     */
    @Override
    public User findByDNI(String dni) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Si el id del usuario es menor que 0 indica que el usuario no existe en la bd
     * por lo que llevamos a cabo la inserción del mismo, en caso contrarario llevaremos
     * a cabo la actualización del mismo.
     *
     * @param user
     * @return
     */
    @Override
    public boolean save(User user) {
        if (user.getId() > 0) {
            return update(user);
        }
        return insert(user);
    }

    /**
     * Elimina el usuario de la base de datos haciendo uso del identificador
     *
     * @param user
     * @return
     */
    public boolean remove(User user) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Actualiza todos los datos del usuario excepto el id
     *
     * @param user
     * @return
     */
    private boolean update(User user) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Inserta el usuario en la base de datos. Deberemos tener en cuenta que, tras la inserción
     * deberemos establecer el id del usuario que la base de datos le ha asignado llamando
     * al correspondiente setter --> user.setID(idAsignado)
     *
     * @param user
     * @return
     */
    private boolean insert(User user) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo email coincide con @email
     * @param email
     * @return
     */
    @Override
    public User findByEmail(String email) {
        throw new RuntimeException("No yet implemented");
    }

    private User mapFromResulset(ResultSet rs) throws SQLException {
        throw new RuntimeException("No yet implemented");
    }

}
