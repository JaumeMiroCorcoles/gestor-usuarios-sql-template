package es.cipfpbatoi.gestorusuariosv2.model.repositories;

import es.cipfpbatoi.gestorusuariosv2.exceptions.NotFoundException;
import es.cipfpbatoi.gestorusuariosv2.model.dao.SQLUserDAO;
import es.cipfpbatoi.gestorusuariosv2.model.dao.interfaces.UserDAO;
import es.cipfpbatoi.gestorusuariosv2.model.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;

@Service
public class UserRepository {
    private UserDAO userDAO;

    public UserRepository(@Autowired SQLUserDAO userDao) {
        this.userDAO = userDao;
    }

    /**
     * Actualiza o inserta un nuevo usuario.
     *
     * @param usuario
     */
    public void save(User usuario) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve totos los usuarios
     * @return
     */
    public ArrayList<User> findAll() {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve todos los usuarios cuyo nombr o dni empiece por @searchField
     *
     * @param searchField
     * @return
     */
    public ArrayList<User> findAll(String searchField) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo dni coincida con @id
     * @return User
     * @throws NotFoundException si no se encuentra el usuario
     */
    public User getByDNI(String dni) throws NotFoundException {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo dni coincida con @dni
     * @param dni
     * @return User or null si el usuario no es encontrado
     */
    private User findByDNI(String dni) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo id coincida con @id
     * @param id
     * @return
     * @throws NotFoundException si no se encuentra el usuario
     */
    public User getById(int id) throws NotFoundException {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Devuelve el usuario cuyo id coincida con @id
     * @param id
     * @return null si el usuario no es encontrado
     */
    private User findById(int id) {
        throw new RuntimeException("No yet implemented");
    }

    /**
     * Borra el usuario
     *
     * @param user
     * @throws NotFoundException si el usuario no existe
     */
    public void remove(User user) throws NotFoundException {
        throw new RuntimeException("No yet implemented");
    }

}
